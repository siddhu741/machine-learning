import numpy as np
from matplotlib import pyplot as plt
from numpy.linalg import inv

#irisData = np.genfromtxt('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', delimiter=",")
irisData = np.genfromtxt('iris.data', delimiter=",")

setosa = irisData[:50,:4]
versicolor = irisData[50:100, :4]
virginica = irisData[100:, :4]
meanSetosa = setosa.mean(0).reshape(4,1)
meanVersicolor = versicolor.mean(0).reshape(4,1)
meanVirginica = virginica.mean(0).reshape(4,1)

def sumIntoSumT(a, u):
    return np.matmul((a-u), ((a-u).transpose()))

Sw = sumIntoSumT(setosa.transpose(), meanSetosa) + sumIntoSumT(versicolor.transpose(), meanVersicolor)
invSw = inv(Sw)
W = np.matmul(invSw, meanVersicolor - meanSetosa)
plt.plot(np.matmul(W.transpose(), versicolor.transpose()), 'x')
plt.plot(np.matmul(W.transpose(), setosa.transpose()), 'o')
plt.title('versicolor vs setosa')
threshold = (np.matmul(W.transpose(), versicolor.transpose()).mean() + np.matmul(W.transpose(), setosa.transpose()).mean() )/2
versicolorValues = np.matmul(W.transpose(), versicolor.transpose())
setosaValues = np.matmul(W.transpose(), setosa.transpose())
misClassified = 0
for i in versicolorValues.transpose():
    if i < threshold:
        misClassified=misClassified +1
for i in setosaValues.transpose():
    if i > threshold:
        misClassified=misClassified +1
print("misclassified points are: ", misClassified)
print("threshold of setosa and versicolor is " , threshold)



Sw2 = sumIntoSumT(virginica.transpose(), meanVirginica) + sumIntoSumT(versicolor.transpose(), meanVersicolor)
invSw2 = inv(Sw2)
W2 = np.matmul(invSw2, meanVersicolor - meanVirginica)
plt.plot(np.matmul(W2.transpose(), versicolor.transpose()), 'x')
plt.plot(np.matmul(W2.transpose(), virginica.transpose()), 'o')
plt.title('versicolor vs virginica')
threshold2 = (np.matmul(W2.transpose(), versicolor.transpose()).mean() + np.matmul(W2.transpose(), virginica.transpose()).mean() )/2

versicolorValues2 = np.matmul(W2.transpose(), versicolor.transpose())
virginicaValues2 = np.matmul(W2.transpose(), virginica.transpose())
misClassified2 = 0
for i in versicolorValues2.transpose():
    if i < threshold2:
        misClassified2=misClassified2 +1
for i in virginicaValues2.transpose():
    if i > threshold2:
        misClassified2=misClassified2 +1
print("misclassified points of virginica and versicolor are: ", misClassified2)
print("threshold of virginica and versicolor is " , threshold2)



Sw3 = sumIntoSumT(virginica.transpose(), meanVirginica) + sumIntoSumT(setosa.transpose(), meanSetosa)
invSw3 = inv(Sw3)
W3 = np.matmul(invSw3, meanVirginica - meanSetosa)
plt.plot(np.matmul(W3.transpose(), virginica.transpose()), 'x')
plt.plot(np.matmul(W3.transpose(), setosa.transpose()), 'o')
plt.title('setosa vs virginica')
threshold3 = (np.matmul(W3.transpose(), virginica.transpose()).mean() + np.matmul(W3.transpose(), setosa.transpose()).mean() )/2

setosaValues3 = np.matmul(W3.transpose(), setosa.transpose())
virginicaValues3 = np.matmul(W3.transpose(), virginica.transpose())
misClassified3 = 0
for i in setosaValues3.transpose():
    if i > threshold3:
        misClassified3=misClassified3 +1
for i in virginicaValues3.transpose():
    if i < threshold3:
        misClassified3=misClassified3 +1
print("misclassified points of virginica and setosa are: ", misClassified3)
print("threshold of setosa and virginica is " , threshold3)
